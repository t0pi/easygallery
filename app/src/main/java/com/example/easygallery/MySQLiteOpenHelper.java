package com.example.easygallery;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.ArrayList;

public class MySQLiteOpenHelper extends SQLiteOpenHelper {

    //Propriétés
    private static final String NUM_BASE = "easygallery.db";
    private static final int VERSION_BASE = 1;
    private static final String NOM_TAB = "easygallery";
    private static final String COL_ID = "id";
    private static final String COL_PATH = "path";
    private static final String COL_THEME = "theme";
    private static final String COL_DESCR = "description";
    private String creation = "CREATE TABLE " + NOM_TAB + "("
            + COL_ID + " INTEGER PRIMARY KEY,"
            + COL_PATH + " TEXT NOT NULL,"
            + COL_THEME + " TEXT NOT NULL,"
            + COL_DESCR + " TEXT NOT NULL);";

    public MySQLiteOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int VERSION_BASE){
        super(context, NUM_BASE, factory, VERSION_BASE);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //création de la table au premier lancement de l'instance
        db.execSQL(creation);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + NOM_TAB);
        onCreate(db);
    }

    public void addImg(updateDb img) {
        // création d'un objet img qui contient le chemin et la description d'une image, ajout dans la bdd
        ContentValues values = new ContentValues();
        values.put(COL_PATH,img.getPath());
        values.put(COL_THEME, img.getDescription());
        values.put(COL_DESCR, img.getDescription());
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(NOM_TAB, null, values);
        db.close();
    }

    public void updateImg (String path, String theme, String description) {
        String query = "UPDATE " + NOM_TAB + " SET " + COL_THEME + " = '" + theme + "', " + COL_DESCR + " = '" + description + "' WHERE " + COL_PATH + " LIKE '" + path + "';";
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(query);
    }

    public String fetchImgPath(String path) {
        String result = "";
        String query = "Select "+ COL_PATH +" FROM " + NOM_TAB + " WHERE " + COL_PATH + " LIKE " + "'" +path+"'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query,null);
        if (cursor.moveToNext()) {
            result = cursor.getString(0);
        }
        cursor.close();
        db.close();
        return result;
    }

    public String fetchImgDescription(String path) {
        String result = "";
        String query = "Select "+ COL_DESCR +" FROM " + NOM_TAB + " WHERE " + COL_PATH + " LIKE '" + path + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query,null);
        if (cursor.moveToNext()) {
            result = cursor.getString(0);
        }
        cursor.close();
        db.close();
        System.out.println(result);
        return result;
    }

    public ArrayList fetchImg() {
        ArrayList<Model_images> imageArray = new ArrayList<>();
        String query = "Select * FROM " + NOM_TAB;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query,null);
        while (cursor.moveToNext()) {
            if (!cursor.getString(2) .equals("")){
                ArrayList<String> al_path = new ArrayList<>();
                al_path.add(cursor.getString(1));
                Model_images obj_model = new Model_images();
                obj_model.setStr_folder(cursor.getString(2));
                obj_model.setAl_imagepath(al_path);

                imageArray.add(obj_model);
            }

        }
        cursor.close();
        db.close();
        return imageArray;
    }
}
