package com.example.easygallery;

public class updateDb {
    private String path;
    private String theme;
    private String description;

    public updateDb(String path, String theme, String description) {
        this.path = path;
        this.theme = theme;
        this.description = description;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
