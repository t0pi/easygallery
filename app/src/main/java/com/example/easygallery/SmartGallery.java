package com.example.easygallery;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * Cette activité permet de détecter les éléments présents sur chaque image sous forme de description et d'ajouter celle ci aux métadonnées de l'image, les images sont ensuite triées par thèmes.
 * Ici, comme sur les autres activités, on appelle la topbar personnalisée (le Titre de l'application avec la barre de recherche)
 * On initialise aussi la barre de boutons (onglets) qui permettent de naviguer sur les autres activités
 *
 * **/


public class SmartGallery extends AppCompatActivity {

    public static ArrayList<Model_images> al_images = new ArrayList<>();
    public static ArrayList<Model_images> smart_images = new ArrayList<>();
    public ArrayList<Model_images> fn_imagespath = null;
    boolean boolean_folder;
    Adapter_PhotosFolder obj_adapter;
    GridView gv_folder;
    private static final int REQUEST_PERMISSIONS = 100;

    // initialisation des variables pour récupérer les éléments du layout (boutons, textes, progressbar)
    private ProgressBar progress;
    ImageView todoImg = null;
    Button updateBtn = null;
    private String captext = null;
    private String[]detectiontext1 = null;
    Context context = null;
    CaptionGenerator captionGenerator = null;
    ObjectClassifier objectClassifier = null;
    String combineDetectionText = null;
    RelativeLayout rl = null;

    //appel de la librairie tensorflow (détection d'image)
    static{
        System.loadLibrary("tensorflow_inference");
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smart_gallery);

        // topbar customisée
        this.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.custom_action_bar);
        getSupportActionBar().setElevation(0);
        View view = getSupportActionBar().getCustomView();
        TextView name = view.findViewById(R.id.name);
        name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(SmartGallery.this, " ", Toast.LENGTH_LONG).show();
            }
        });

        // Bouton pour aller sur Pictures (main)
        Button PicturesButton = findViewById(R.id.goPictures);
        PicturesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SmartGallery.this, MainActivity.class));

            }
        });

        // Bouton pour aller sur Albums
        Button albumButton = findViewById(R.id.goAlbums);
        albumButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SmartGallery.this, Albums.class));

            }
        });

        gv_folder = findViewById(R.id.gv_folder);
        // on récupère les élements du layout
        progress = (ProgressBar)findViewById(R.id.progressBar);
        progress.setVisibility(View.INVISIBLE);
        context= this.getApplicationContext();
        updateBtn = (Button)findViewById(R.id.update);
        todoImg = findViewById(R.id.todoImg);
        rl = findViewById(R.id.rl);

        /**
         *
         * SMARTGALLERY
         *
         * **/

        /** Vérification des permissions de lecture et écriture **/
        if ((ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
            if ((ActivityCompat.shouldShowRequestPermissionRationale(SmartGallery.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) && (ActivityCompat.shouldShowRequestPermissionRationale(SmartGallery.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE))) {

            } else {
                ActivityCompat.requestPermissions(SmartGallery.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_PERMISSIONS);
            }
        } else {
            Log.e("Else", "Else");
            fn_imagespath();
            smartFolders();
        }


        /*Initialize Models*/

        Runnable runnable1 = new Runnable() {
            @Override
            public void run() {
                progress.setVisibility(View.VISIBLE);//initialisation
                updateBtn.setVisibility(View.INVISIBLE);
                rl.setVisibility(View.INVISIBLE);

                try {
                    objectClassifier = new ObjectClassifier(context);
                    captionGenerator = new CaptionGenerator(context);
                }catch (IOException e){
                    e.printStackTrace();
                }
                progress.post(new Runnable() {
                    @Override
                    public void run() {
                        progress.setVisibility(View.INVISIBLE);
                        updateBtn.setVisibility(View.VISIBLE);
                        rl.setVisibility(View.VISIBLE);
                    }
                });
            }
        };
        new Thread(runnable1).start();

    }



    /** On récupère les chemin de chaque image grace à mediastore et on les stocke dans un arraylist, on les affiche ensuite grace à Adapter_PhotosFolder **/
    public ArrayList<Model_images> fn_imagespath() {
        al_images.clear();

        int int_position = 0;
        Uri uri;
        Cursor cursor;
        int column_index_data, column_index_folder_name;

        String absolutePathOfImage = null;
        uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        String[] projection = {MediaStore.MediaColumns.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME};

        final String orderBy = MediaStore.Images.Media.DATE_TAKEN;
        cursor = getApplicationContext().getContentResolver().query(
                uri,
                projection,
                MediaStore.Images.Media.DATA + " like ? ",
                new String[] {"%EasyGalleryTestImages%"},
                orderBy + " DESC"
        );

        column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        column_index_folder_name = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
        while (cursor.moveToNext()) {
            absolutePathOfImage = cursor.getString(column_index_data);

            for (int i = 0; i < al_images.size(); i++) {
                if (al_images.get(i).getStr_folder().equals(cursor.getString(column_index_folder_name))) {
                    boolean_folder = true;
                    int_position = i;
                    break;
                } else {
                    boolean_folder = false;
                }
            }

            if (boolean_folder) {
                ArrayList<String> al_path = new ArrayList<>();
                al_path.addAll(al_images.get(int_position).getAl_imagepath());
                al_path.add(absolutePathOfImage);
                al_images.get(int_position).setAl_imagepath(al_path);
            }
            else {
                ArrayList<String> al_path = new ArrayList<>();
                al_path.add(absolutePathOfImage);
                Model_images obj_model = new Model_images();
                obj_model.setStr_folder(cursor.getString(column_index_folder_name));
                obj_model.setAl_imagepath(al_path);

                al_images.add(obj_model);
            }
        }

        return al_images;
    }

    public ArrayList<Model_images> smartFolders() {
        smart_images.clear();
        /** Création de l'objet mySQLiteOpenHelper **/
        MySQLiteOpenHelper mySQLiteOpenHelper = new MySQLiteOpenHelper(this,null,null,1);
        smart_images = mySQLiteOpenHelper.fetchImg();
        for (int i = 0; i < smart_images.size(); i++) {
            Log.e("FOLDER", smart_images.get(i).getStr_folder());
            for (int j = 0; j < smart_images.get(i).getAl_imagepath().size(); j++) {
                Log.e("FILE", smart_images.get(i).getAl_imagepath().get(j));
            }
        }

        obj_adapter = new Adapter_PhotosFolder(getApplicationContext(),smart_images);
        gv_folder.setAdapter(obj_adapter);
        return smart_images;
    }

    /** Permissions **/
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_PERMISSIONS: {
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults.length > 0 && grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        fn_imagespath();
                    } else {
                        Toast.makeText(SmartGallery.this, "The app was not allowed to read or write to your storage. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show();
                    }
                }
            }
        }

    }

    public void scanImages(View view){

        System.out.println("********************scanImage...");

        String imgPath;
        String imgTheme = "";
        String imgDescription = "";


        /** boucle pour récupérer tous les chemins de chaque image présente dans le répertoire grace au arraylist "al_images" **/
        for (int i = 0; i < al_images.size(); i++) {
            for (int j = 0; j < al_images.get(i).getAl_imagepath().size(); j++) {

                imgPath = al_images.get(i).getAl_imagepath().get(j);//on récupère chaque path

                /** Création de l'objet mySQLiteOpenHelper **/
                MySQLiteOpenHelper mySQLiteOpenHelper = new MySQLiteOpenHelper(this,null,null,1);

                /** si l'image n'est pas présente dans la bdd (path) **/
                if (!mySQLiteOpenHelper.fetchImgPath(imgPath).equals(imgPath)) {
                    /** On l'ajoute dans la bdd **/
                    updateDb newImg = new updateDb(imgPath, imgTheme, imgDescription);
                    mySQLiteOpenHelper.addImg(newImg);
                }

                /** si l'image est présente dans la bdd mais que sa description est vide **/
                else if (mySQLiteOpenHelper.fetchImgPath(imgPath).equals(imgPath) && mySQLiteOpenHelper.fetchImgDescription(imgPath).equals("")){
                    /** On affiche l'image dans un imageView pour qu'elle soit traitée par les détecteurs **/
                    FileInputStream fileInputStream;
                    try {
                        fileInputStream = new FileInputStream(imgPath);
                        Bitmap bitmap = BitmapFactory.decodeStream(fileInputStream);
                        todoImg.setImageBitmap(bitmap);
                        fileInputStream.close();

                    } catch (FileNotFoundException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                        Log.d ("Incident", "Fichier non trouvé !!!");
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    /** On lance ObjectClassifier  **/

                    progress.setVisibility(View.VISIBLE);//initialisation
                    Bitmap bitmap = ((BitmapDrawable) todoImg.getDrawable()).getBitmap();


                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG,90,baos);
                    byte [] imageInByte = baos.toByteArray();

                    try{
                        baos.close();
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                    final Bitmap bitmapnew = BitmapFactory.decodeByteArray(imageInByte,0,imageInByte.length);
                    detectiontext1 = objectClassifier.runModel(bitmapnew);

                    for(int e = 0 ; e < detectiontext1.length; e++){
                        if(detectiontext1[e]!= null){
                            combineDetectionText = detectiontext1[e];
                            combineDetectionText += ' ';
                        }
                    }
                    String detectedObject = combineDetectionText;
                    String detectedObjectTheme = ObjectClassifier.getTheme();

                    /** On lance CaptionGenerator  **/
                    captext = captionGenerator.runModel(bitmapnew);
                    String capTheme = CaptionGenerator.getTheme();

                    /** On regroupe les thèmes et tags détectés par ObjectClassifier et CaptionGenerator **/

                    if (!capTheme .equals("")){

                        imgTheme = capTheme;
                    }
                    else {
                        imgTheme = detectedObjectTheme;
                    }
                    String tempimgDescription = combineDetectionText + " " + captext;

                    imgDescription = tempimgDescription.replace( '\'', ' ' );//si la description contient des apostrophes

                    /** On met la base de données à jour  **/
                    progress.setVisibility(View.INVISIBLE);//initialisation
                    mySQLiteOpenHelper.updateImg(imgPath, imgTheme, imgDescription);
                }
                /** Si l'image existe dans la bdd et que sa description n'est pas vide **/
                else if (mySQLiteOpenHelper.fetchImgPath(imgPath).equals(imgPath) && !mySQLiteOpenHelper.fetchImgDescription(imgPath).equals("")){
                    Toast.makeText(SmartGallery.this, "No new pictures found", Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
